# Sutty nameserver

Non-compliant RFC nameserver, it only redirects `_dnslink.*` queries to
[Distributed Press](https://distributed.press/) nameservers, since
wildcards can only be use on the left side only.

## Usage

Download the latest
[release](https://0xacab.org/sutty/suttyns/-/releases).

```bash
suttyns --help
```

## Contributing

1. Fork it (<https://0xacab.org/sutty/suttyns/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [f](https://0xacab.org/fauno) - creator and maintainer
