# Copyright 2023 Cooperativa de Trabajo Sutty Ltda.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require "socket"
require "mDNS/mdns"
require "option_parser"
require "log"

# Port is an unsigned integer on the range of 1 to 65535
port : UInt16 = 53535
# Address can be any local IPv4 or IPv6 address.  Use "::" for any
# address.
addr = "::"
# Redirect requests to Distributed Press
dist = "api.distributed.press."
# Address family
inet = Socket::Family::INET6
# Public IPv4 address
addr4 = "127.0.0.1"

OptionParser.parse do |p|
  p.banner = "Sutty nameserver\n\nUsage: suttyns [-h] [-a #{addr}] [-p #{port}] [-d #{dist}] [-4 #{addr4}]"

  p.on("-h", "--help", "Help") do
    puts p
    exit
  end

  p.on("-a ADDRESS", "--address=ADDRESS", "Bind IP address, default all interfaces") do |a|
    addr = a
    inet = Socket::IPAddress.new(addr, port).family
  end

  p.on("-p PORT", "--port=PORT", "Port") do |p|
    p.to_u16.tap do |pu|
      raise ArgumentError.new("") if pu.zero?

      port = pu
    end
  rescue ArgumentError
    Log.warn &.emit("Port out of range", port: p)
  end

  p.on("-d HOSTNAME", "--distributed-press=HOSTNAME", "Distributed Press nameserver") do |h|
    dist = String.build do |s|
      s << h
      s << "." unless h.ends_with? "."
    end
  end

  p.on("-4 ADDR4", "--ipv4=ADDR4", "Public IPv4 address") do |i|
    addr4 = i
  end
end

# Create a UDP socket for this nameserver.
begin
  socket = UDPSocket.new inet
  socket.reuse_address = true
  socket.reuse_port = true
  socket.bind addr, port
rescue Socket::BindError
  Log.error &.emit("Bind error, check address or port", address: addr, port: port.to_s)
  exit 1
end

# Encode Distributed Press' resource record
press = MDNS::DomainNamePointer.new
press.domain_name = dist
data = press.to_slice

addr4_bytes = Bytes.new(4)
addr4.split(".").each_with_index { |component, index| addr4_bytes[index] = component.to_u8 }

Log.info &.emit("Listening", address: addr, port: port.to_s)
Log.info &.emit("Authoritative _dnslink provider", provider: dist)

# Run forever
loop do
  if socket.closed?
    Log.warn { "Socket closed, exiting" }
    break
  end

  # Read up to 512 bytes per request
  buffer = Bytes.new(512)
  size, address = socket.receive(buffer)

  # Ignore empty requests
  if size == 0
    Log.warn { "Read 0 bytes" }
    next
  end

  # Parse the request and start a response message
  io = IO::Memory.new(buffer[0...size].clone)
  message = io.read_bytes(MDNS::Message).set_io(io)
  message.is_response = true
  message.authoritative_answer = true

  message.queries.each do |q|
    resource = MDNS::Resource.new
    resource.ttl = 1.minute
    resource.domain_name = q.domain_name

    # Ignore anything that's not a _dnslink record.
    if q.domain_name.downcase.starts_with?("_dnslink.")
      Log.info &.emit("Redirecting", domain: q.domain_name)

      # Point resolvers to the Distributed Press server for the actual TXT
      # resolution.
      resource.type = :ns
      resource.data = data

      # Response is on the authoritative section.
      message.name_servers << resource
    elsif q.type.a?
      Log.info &.emit("Answering", domain: q.domain_name)

      resource.type = :a
      resource.data = addr4_bytes

      message.answers << resource
    # If there were no valid requests we don't have anything to say
    else
      Log.info &.emit("No redirections to perform", domain: q.domain_name, type: q.type.to_s)
      message.response_code = MDNS::ResponseCode::NoError
    end
  end

  # Reply to queries
  socket.send(message, address)

# Rescue from fuzzing
rescue BinData::ParseError
  Log.error { "Parsing error" }
  next
end
